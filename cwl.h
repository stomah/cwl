#pragma once

#include <inttypes.h>
#include <stddef.h>

#define cwl_STRUCT(name) typedef struct name name; struct name
#define cwl_UNION(name) typedef union name name; union name
#define cwl_ENUM(name) typedef enum name name; enum name

cwl_STRUCT(cwlString) {
	size_t size;
	char const *bytes;
};

#define cwl_STR(literal) (cwlString) { sizeof(literal) - 1, literal }


cwl_ENUM(cwlWindowFlags) {
	cwlWindowFlags_RESIZABLE = 1 << 0,
	cwlWindowFlags_TITLED = 1 << 1,
	cwlWindowFlags_CLOSABLE = 1 << 2,
};


cwl_STRUCT(cwlInstance);
cwl_STRUCT(cwlWindow);


cwlInstance *cwlInstance_create(void);
void cwlInstance_destroy(cwlInstance *);

char const *cwlInstance_get_vulkan_surface_extension(cwlInstance *);

void cwlInstance_run(cwlInstance *);
void cwlInstance_terminate(cwlInstance *);

cwlWindow */*opt*/cwlInstance_get_hovered_window(cwlInstance *);
double cwlInstance_get_pointer_x(cwlInstance *);   // undefined if no hovered window
double cwlInstance_get_pointer_y(cwlInstance *);   // undefined if no hovered window
unsigned cwlInstance_get_pressed_pointer_buttons(cwlInstance *);
	// bitmask of pressed pointer buttons; 0 if no hovered window



cwlWindow *cwlWindow_create(cwlInstance *, cwlWindowFlags, void *user_data);
cwlInstance *cwlWindow_get_instance(cwlWindow *);
void cwlWindow_destroy(cwlWindow *);

void cwlWindow_set_user_data(cwlWindow *, void *);
void *cwlWindow_get_user_data(cwlWindow *);

struct VkInstance_T;
struct VkSurfaceKHR_T;
// returns NULL if failed to create vulkan surface
struct VkSurfaceKHR_T */*opt*/cwlWindow_create_vulkan_surface(cwlWindow *, struct VkInstance_T *);
_Bool cwlWindow_should_use_mailbox_present_mode(cwlWindow *);

// the changes are not guarranteed to take effect before the next present {
void cwlWindow_set_title(cwlWindow *, cwlString);
void cwlWindow_set_size(cwlWindow *, unsigned width, unsigned height);   // in pixels
// }

_Bool cwlWindow_has_client_side_decoration(cwlWindow *);
void cwlWindow_begin_move(cwlWindow *);   // must be in response to an event

cwl_ENUM(cwlPointerButton) {
	cwlPointerButton_PRIMARY,
	cwlPointerButton_SECONDARY,
	cwlPointerButton_MIDDLE,
	cwlPointerButton_BACK,
	cwlPointerButton_FORWARD,
};

cwl_ENUM(cwlPointerAxisSource) {
	cwlPointerAxisSource_UNKNOWN,
	cwlPointerAxisSource_WHEEL,
	cwlPointerAxisSource_FINGER,
	cwlPointerAxisSource_CONTINUOUS,
};

cwl_ENUM(cwlKeyEventType) {
	cwlKeyEventType_RELEASE,
	cwlKeyEventType_PRESS,
	cwlKeyEventType_REPEAT,
};

cwl_ENUM(cwlWindowStateFlags) {
	cwlWindowStateFlags_MAXIMIZED = 1 << 0,
	cwlWindowStateFlags_FULLSCREEN = 1 << 1,
	cwlWindowStateFlags_ACTIVE = 1 << 2,
	cwlWindowStateFlags_CLIENT_SIDE_DECORATION = 1 << 3,
};

typedef void (*cwlWindowCloseHandler)(cwlWindow *);
typedef void (*cwlWindowResizeHandler)(
	cwlWindow *, unsigned width, unsigned height, float scale, cwlWindowStateFlags);
	// width and height are the preferred size of the window in pixels.
	// If width and height are 0, there is no preferred size.
	// If the window is maximized or fullscreen, the client must use exactly the specified size.
	// scale is the preferred number of pixels per logical pixel.
	// The client decides the scale by `cwlWindow_set_size` and the size of the presented surface.
typedef void (*cwlWindowFrameHandler)(cwlWindow *, unsigned time);
	// called to start rendering a new frame
typedef void (*cwlPointerMotionHandler)(cwlWindow *, unsigned time, double x, double y);
typedef void (*cwlPointerButtonHandler)(cwlWindow *, unsigned time, cwlPointerButton, _Bool state);
typedef void (*cwlPointerAxisHandler)(
	cwlWindow *, unsigned time, double x, double y, cwlPointerAxisSource);   // x and y are in pixels
typedef void (*cwlKeyHandler)(cwlWindow *, unsigned time, unsigned key_code, cwlKeyEventType);

void cwlWindow_on_close(cwlWindow *, cwlWindowCloseHandler);
void cwlWindow_on_resize(cwlWindow *, cwlWindowResizeHandler);
void cwlWindow_on_frame(cwlWindow *, cwlWindowFrameHandler);
void cwlWindow_on_pointer_move(cwlWindow *, cwlPointerMotionHandler);
void cwlWindow_on_pointer_button(cwlWindow *, cwlPointerButtonHandler);
void cwlWindow_on_pointer_axis(cwlWindow *, cwlPointerAxisHandler);
void cwlWindow_on_key(cwlWindow *, cwlKeyHandler);
