set -e
mkdir -p /usr/local/include/
cp cwl.h /usr/local/include/
mkdir -p /usr/local/lib/pkgconfig/
cp libcwl.a /usr/local/lib/

if [[ $OSTYPE == 'darwin'* ]]; then
	cp cwl_macos.pc /usr/local/lib/pkgconfig/cwl.pc
else
	cp cwl_wayland.pc /usr/local/lib/pkgconfig/cwl.pc
fi
