#include "cwl.h"

#include "internal_common.h"

#include <wayland-client.h>
#include <wayland-server.h>
#include <wayland-cursor.h>
#include "xdg-shell-client.h"
#include <xkbcommon/xkbcommon.h>

#include <sys/mman.h>
#include <unistd.h>
#include <linux/input-event-codes.h>

#include <vulkan/vulkan.h>
#include <vulkan/vulkan_wayland.h>

#define REQUIRED_WL_COMPOSITOR_VERSION 5
#define REQUIRED_WL_SEAT_VERSION 8
#define REQUIRED_XDG_WM_BASE_VERSION 4

STRUCT(cwlInstance) {
	struct wl_event_loop *event_loop;
	bool terminate;
	
	struct wl_display *display;
	struct wl_compositor *compositor;
	struct xdg_wm_base *xdg_wm_base;
	struct wl_seat *seat;
	struct wl_shm *shm;
	
	struct wl_cursor_theme *cursor_theme;
	
	struct xkb_context *xkb_context;
	
	uint event_serial;
	
	struct wl_pointer */*opt*/pointer;
	cwlWindow */*opt*/pointer_window;
	double pointer_x, pointer_y;
	uint pressed_pointer_buttons;
	uint pointer_event_time;
	double pointer_axis_x, pointer_axis_y;
	cwlPointerAxisSource pointer_axis_source;
	
	struct wl_keyboard */*opt*/keyboard;
	cwlWindow */*opt*/keyboard_window;
	struct xkb_keymap *keymap;
	struct xkb_state *state;
	uint16_t repeat_start_delay_ms;
	uint16_t repeat_delay_ms;
	uint repeat_key;
	struct wl_event_source *repeat_timer;
};

STRUCT(cwlWindow) {
	cwlInstance *instance;
	void *user_data;
	
#define O(name, type) type name;
	ENUMERATE_WINDOW_CALLBACKS(O)
#undef O
	
	struct wl_surface *wl_surface;        // data points to window
	struct wl_callback */*opt*/frame_callback;   // data points to window
	struct xdg_surface *xdg_surface;      // data points to window
	struct xdg_toplevel *toplevel;      // data points to window
	
	uint configure_width;
	uint configure_height;
};


static void pointer_enter(void *instance_, struct wl_pointer *,
	uint, struct wl_surface *wl_surface, wl_fixed_t, wl_fixed_t)
{
	cwlInstance *instance = instance_;
	instance->pointer_window = wl_surface_get_user_data(wl_surface);
}

static void pointer_leave(void *instance_, struct wl_pointer *, uint, struct wl_surface *) {
	cwlInstance *instance = instance_;
	instance->pointer_window = NULL;
}

static void pointer_motion(void *instance_, struct wl_pointer *,
	uint time, wl_fixed_t x, wl_fixed_t y)
{
	cwlInstance *instance = instance_;
	instance->pointer_x = wl_fixed_to_double(x);
	instance->pointer_y = wl_fixed_to_double(y);
	instance->pointer_window->on_pointer_move(instance->pointer_window,
		time, instance->pointer_x, instance->pointer_y);
}

static void pointer_button(void *instance_, struct wl_pointer *,
	uint serial, uint time, uint button, uint state)
{
	cwlInstance *instance = instance_;
	
	cwlPointerButton b;
	switch (button) {
		case BTN_LEFT  : b = cwlPointerButton_PRIMARY  ; break;
		case BTN_RIGHT : b = cwlPointerButton_SECONDARY; break;
		case BTN_MIDDLE: b = cwlPointerButton_MIDDLE   ; break;
		case BTN_SIDE  : b = cwlPointerButton_BACK     ; break;
		case BTN_EXTRA : b = cwlPointerButton_FORWARD  ; break;
		default: fprintf(stderr, "TODO: unhandled pointer button %#x\n", button); return;
	}
	
	if (state)
		instance->pressed_pointer_buttons |= 1 << b;
	else
		instance->pressed_pointer_buttons &= ~(1 << b);
	
	instance->event_serial = serial;
	instance->pointer_window->on_pointer_button(instance->pointer_window, time, b, state);
	instance->event_serial = 0;
}

static void pointer_axis(void *instance_, struct wl_pointer *,
	uint time, uint axis, wl_fixed_t value)
{
	cwlInstance *instance = instance_;
	if (axis == WL_POINTER_AXIS_HORIZONTAL_SCROLL)
		instance->pointer_axis_x += wl_fixed_to_double(value);
	else if (axis == WL_POINTER_AXIS_VERTICAL_SCROLL)
		instance->pointer_axis_y += wl_fixed_to_double(value);
	instance->pointer_event_time = time;
}

static void pointer_frame(void *instance_, struct wl_pointer *) {
	cwlInstance *instance = instance_;
	
	if (instance->pointer_axis_x != 0 || instance->pointer_axis_y != 0) {
		instance->pointer_window->on_pointer_axis(instance->pointer_window,
			instance->pointer_event_time, instance->pointer_axis_x, instance->pointer_axis_y,
			instance->pointer_axis_source);
		
		instance->pointer_axis_x = 0;
		instance->pointer_axis_y = 0;
		instance->pointer_axis_source = cwlPointerAxisSource_UNKNOWN;
	}
}

static void pointer_axis_source(void *instance_, struct wl_pointer *, uint axis_source) {
	cwlInstance *instance = instance_;
	
	if (axis_source == WL_POINTER_AXIS_SOURCE_WHEEL)
		instance->pointer_axis_source = cwlPointerAxisSource_WHEEL;
	if (axis_source == WL_POINTER_AXIS_SOURCE_FINGER)
		instance->pointer_axis_source = cwlPointerAxisSource_FINGER;
	if (axis_source == WL_POINTER_AXIS_SOURCE_CONTINUOUS)
		instance->pointer_axis_source = cwlPointerAxisSource_CONTINUOUS;
}

static void pointer_axis_stop(void *, struct wl_pointer *, uint, uint) {}

static void pointer_axis_discrete(void *, struct wl_pointer *, uint, int) {}

static void pointer_axis_value120(void *, struct wl_pointer *, uint, int) {}

static void pointer_axis_relative_direction(void *, struct wl_pointer *, uint, uint) {}

static struct wl_pointer_listener const pointer_listener = {
	pointer_enter, pointer_leave, pointer_motion, pointer_button, pointer_axis, pointer_frame,
	pointer_axis_source, pointer_axis_stop, pointer_axis_discrete, pointer_axis_value120,
	pointer_axis_relative_direction,
};


static void keyboard_keymap(void *instance_, struct wl_keyboard *, uint format, int fd, uint size) {
	cwlInstance *instance = instance_;
	
	if (format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1)
		die("keymap format is not xkb v1");
	
	void *keymap = mmap(0, size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (!keymap)
		die("failed to map keymap");
	
	instance->keymap = xkb_keymap_new_from_buffer(instance->xkb_context,
		keymap, size - 1, XKB_KEYMAP_FORMAT_TEXT_V1, 0);
	if (!instance->keymap)
		die("failed to compile keymap");
	
	munmap(keymap, size);
	close(fd);
	
	instance->state = xkb_state_new(instance->keymap);
	if (!instance->state)
		die("failed to create xkb state");
}

static void keyboard_enter(void *instance_, struct wl_keyboard *,
	uint, struct wl_surface *surface, struct wl_array *)
{
	cwlInstance *instance = instance_;
	instance->keyboard_window = wl_surface_get_user_data(surface);
}

static void keyboard_leave(void *instance_, struct wl_keyboard *, uint, struct wl_surface *) {
	cwlInstance *instance = instance_;
	instance->keyboard_window = NULL;
}

static void keyboard_repeat_sync(void *instance_, struct wl_callback *, uint serial);

static struct wl_callback_listener const repeat_listener = { keyboard_repeat_sync };

static void keyboard_repeat_sync(void *instance_, struct wl_callback *callback, uint) {
	cwlInstance *instance = instance_;
	wl_callback_destroy(callback);
	if (instance->repeat_key == 0)
		wl_event_source_timer_update(instance->repeat_timer, 0);
	else
		instance->keyboard_window->on_key(
			instance->keyboard_window, 0, instance->repeat_key, cwlKeyEventType_REPEAT);
}

static int keyboard_repeat(void *instance_) {
	cwlInstance *instance = instance_;
	struct wl_callback *sync_callback = wl_display_sync(instance->display);
	wl_callback_add_listener(sync_callback, &repeat_listener, instance);
	wl_event_source_timer_update(instance->repeat_timer, instance->repeat_delay_ms);
	return 0;
}

static void keyboard_key(
	void *instance_, struct wl_keyboard *, uint, uint time, uint key, uint state)
{
	cwlInstance *instance = instance_;
	instance->keyboard_window->on_key(instance->keyboard_window, time, key, state);
	
	if (state && xkb_keymap_key_repeats(instance->keymap, key + 8) && instance->repeat_delay_ms > 0) {
		instance->repeat_key = key;
		wl_event_source_timer_update(instance->repeat_timer, instance->repeat_start_delay_ms);
	} else if (key == instance->repeat_key) {
		wl_event_source_timer_update(instance->repeat_timer, 0);
	}
}

static void keyboard_modifiers(void *instance_, struct wl_keyboard *,
	uint, uint depressed, uint latched, uint locked, uint group)
{
	cwlInstance *instance = instance_;
	xkb_state_update_mask(instance->state, depressed, latched, locked, 0, 0, group);
}

static void keyboard_repeat_info(
	void *instance_, struct wl_keyboard *, int rate_per_sec, int delay_ms)
{
	cwlInstance *instance = instance_;
	instance->repeat_start_delay_ms = delay_ms;
	instance->repeat_delay_ms = rate_per_sec == 0 ? 0 : 1000 / rate_per_sec;
}

static struct wl_keyboard_listener const keyboard_listener = {
	keyboard_keymap, keyboard_enter, keyboard_leave, keyboard_key,
	keyboard_modifiers, keyboard_repeat_info
};

/*uint uiKeyboard_get_utf8(uiKeyboard *self, uint key, uint buffer_size, byte *buffer) {
	return xkb_state_key_get_utf8(self->state, key + 8, buffer, buffer_size);
}*/


static void seat_capabilities(void *instance_, struct wl_seat *seat, uint capabilities) {
	cwlInstance *instance = instance_;
	if ((capabilities & WL_SEAT_CAPABILITY_POINTER) && instance->pointer == NULL) {
		/*data->cursor_default = wl_cursor_theme_get_cursor(data->cursor_theme, "wait");
		if (!data->cursor_default)
			die("error: could not load cursor");
		
		data->cursor_surface = wl_compositor_create_surface(instance->compositor);*/
		//wl_callback_add_listener(wl_surface_frame(data->cursor_surface), &cursor_frame_listener, pointer);
		
		instance->pointer = wl_seat_get_pointer(seat);
		wl_pointer_add_listener(instance->pointer, &pointer_listener, instance);
	} else if (!(capabilities & WL_SEAT_CAPABILITY_POINTER) && instance->pointer != NULL) {
		wl_pointer_destroy(instance->pointer);
		instance->pointer = NULL;
	}
	
	if ((capabilities & WL_SEAT_CAPABILITY_KEYBOARD) && instance->keyboard == NULL) {
		instance->keyboard = wl_seat_get_keyboard(seat);
		instance->repeat_timer = wl_event_loop_add_timer(
			instance->event_loop, keyboard_repeat, instance);
		wl_keyboard_add_listener(instance->keyboard, &keyboard_listener, instance);
	} else if (!(capabilities & WL_SEAT_CAPABILITY_KEYBOARD) && instance->keyboard != NULL) {
		wl_keyboard_destroy(instance->keyboard);
		instance->keyboard = NULL;
	}
}

static void seat_name(void *, struct wl_seat *, char const *) {}

static struct wl_seat_listener const seat_listener = { seat_capabilities, seat_name };


static void xdg_wm_base_ping(void *, struct xdg_wm_base *xdg_wm_base, uint serial) {
	xdg_wm_base_pong(xdg_wm_base, serial);
}

static const struct xdg_wm_base_listener xdg_wm_base_listener = { xdg_wm_base_ping };


static void registry_add(
	void *instance_, struct wl_registry *registry, uint name, char const *interface, uint version)
{
	cwlInstance *instance = instance_;
	if (!strcmp(interface, wl_compositor_interface.name) && version >= REQUIRED_WL_COMPOSITOR_VERSION) {
		instance->compositor = wl_registry_bind(registry, name,
			&wl_compositor_interface, REQUIRED_WL_COMPOSITOR_VERSION);
	} else
	if (!strcmp(interface, xdg_wm_base_interface.name) && version >= REQUIRED_XDG_WM_BASE_VERSION) {
		instance->xdg_wm_base = wl_registry_bind(registry, name,
			&xdg_wm_base_interface, REQUIRED_XDG_WM_BASE_VERSION);
		xdg_wm_base_add_listener(instance->xdg_wm_base, &xdg_wm_base_listener, NULL);
	} else
	if (!strcmp(interface, wl_seat_interface.name) && version >= REQUIRED_WL_SEAT_VERSION) {
		instance->seat = wl_registry_bind(registry, name, &wl_seat_interface, REQUIRED_WL_SEAT_VERSION);
		wl_seat_add_listener(instance->seat, &seat_listener, instance);
	} else
	if (!strcmp(interface, wl_shm_interface.name)) {
		instance->shm = wl_registry_bind(registry, name, &wl_shm_interface, 1);
		instance->cursor_theme = wl_cursor_theme_load(0, 24, instance->shm);
	}
}

static void registry_remove(void *, struct wl_registry *, uint) {}

static struct wl_registry_listener const registry_listener = { registry_add, registry_remove };


static int dispatch(int, uint32_t, void *display) {
	if (wl_display_dispatch(display) == -1)
		die("wayland display dispatch error");
	return 0;
}

cwlInstance *cwlInstance_create(void) {
	cwlInstance *instance = smalloc(sizeof*instance);
	*instance = (cwlInstance) {};
	
	instance->event_loop = wl_event_loop_create();
	if (!instance->event_loop)
		die("failed to create event loop");
	
	instance->display = wl_display_connect(0);
	if (!instance->display)
		die("failed to connect to wayland display");
	wl_event_loop_add_fd(instance->event_loop, wl_display_get_fd(instance->display),
		WL_EVENT_READABLE | WL_EVENT_WRITABLE, dispatch, instance->display);
	
	struct wl_registry *registry = wl_display_get_registry(instance->display);
	wl_registry_add_listener(registry, &registry_listener, instance);
	wl_display_roundtrip(instance->display);
	if (instance->compositor == NULL || instance->xdg_wm_base == NULL || instance->seat == NULL
	|| instance->shm == NULL)
		die("required wayland globals not present");
	
	instance->xkb_context = xkb_context_new(0);
	if (!instance->xkb_context)
		die("failed to create xkb context");
	
	return instance;
}

char const *cwlInstance_get_vulkan_surface_extension(cwlInstance *) {
	return VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME;
}

void cwlInstance_run(cwlInstance *instance) {
	instance->terminate = false;
	while (!instance->terminate)
		wl_event_loop_dispatch(instance->event_loop, -1);
}

void cwlInstance_terminate(cwlInstance *instance) {
	instance->terminate = true;
}

cwlWindow */*opt*/cwlInstance_get_hovered_window(cwlInstance *instance) {
	return instance->pointer_window;
}

double cwlInstance_get_pointer_x(cwlInstance *instance) {
	return instance->pointer_x;
}

double cwlInstance_get_pointer_y(cwlInstance *instance) {
	return instance->pointer_y;
}

unsigned cwlInstance_get_pressed_pointer_buttons(cwlInstance *instance) {
	return instance->pressed_pointer_buttons;
}

void cwlInstance_destroy(cwlInstance *instance) {
	if (instance->pointer != NULL)
		wl_pointer_destroy(instance->pointer);
	if (instance->keyboard != NULL)
		wl_keyboard_destroy(instance->keyboard);
	
	xkb_context_unref(instance->xkb_context);
	
	wl_cursor_theme_destroy(instance->cursor_theme);
	wl_shm_destroy(instance->shm);
	wl_seat_destroy(instance->seat);
	xdg_wm_base_destroy(instance->xdg_wm_base);
	wl_compositor_destroy(instance->compositor);
	
	wl_event_loop_destroy(instance->event_loop);
	wl_display_disconnect(instance->display);
	
	free(instance);
}


static void frame(void *window_, struct wl_callback *callback, uint time);

static struct wl_callback_listener const frame_listener = { frame };

static void frame(void *window_, struct wl_callback *callback, uint time) {
	cwlWindow *window = window_;
	wl_callback_destroy(callback);
	
	window->frame_callback = wl_surface_frame(window->wl_surface);
	wl_callback_add_listener(window->frame_callback, &frame_listener, window);
	window->on_frame(window, time);
	//wl_surface_commit(surface->wl_surface);
}

static void xdg_surface_configure(void *window_, struct xdg_surface *xdg_surface, uint serial) {
	cwlWindow *window = window_;
	xdg_surface_ack_configure(xdg_surface, serial);
	window->on_resize(window, window->configure_width, window->configure_height, 1, TODO);
	if (window->frame_callback == NULL) {
		window->frame_callback = wl_surface_frame(window->wl_surface);
		wl_callback_add_listener(window->frame_callback, &frame_listener, window);
		
		struct timespec t;
		clock_gettime(CLOCK_MONOTONIC, &t);
		uint time = t.tv_nsec / 1000000 + t.tv_sec * 1000;
		window->on_frame(window, time);
		wl_surface_commit(window->wl_surface);
	}
}

static struct xdg_surface_listener const xdg_surface_listener = { xdg_surface_configure };

static void toplevel_configure(
	void *window_, struct xdg_toplevel *, int width, int height, struct wl_array *)
{
	cwlWindow *window = window_;
	window->configure_width = width;
	window->configure_height = height;
	printf("toplevel configure %dx%d\n", width, height);
}

static void toplevel_close(void *window_, struct xdg_toplevel *) {
	cwlWindow *window = window_;
	window->on_close(window);
}

static void toplevel_configure_bounds(void *window_, struct xdg_toplevel *, int width, int height) {
	cwlWindow *window = window_;
	printf("toplevel configure bounds %dx%d\n", width, height);
	(void)window;
}

static void toplevel_wm_capabilities(void *window_, struct xdg_toplevel *, struct wl_array *) {
	cwlWindow *window = window_;
	printf("toplevel configure WM capabilities\n");
	(void)window;
}

static struct xdg_toplevel_listener const toplevel_listener = {
	toplevel_configure, toplevel_close, toplevel_configure_bounds, toplevel_wm_capabilities };

cwlWindow *cwlWindow_create(cwlInstance *instance, cwlWindowFlags, void *user_data) {
	cwlWindow *window = smalloc(sizeof*window);
	*window = (cwlWindow) {
		.instance = instance,
		.user_data = user_data,
#define O(name, type) .name = name##_default,
		ENUMERATE_WINDOW_CALLBACKS(O)
#undef O
	};
	
	window->wl_surface = wl_compositor_create_surface(instance->compositor);
	wl_surface_set_user_data(window->wl_surface, window);
	//window->frame_callback = wl_surface_frame(window->wl_surface);
	//wl_callback_add_listener(window->frame_callback, &frame_listener, window);
	
	window->xdg_surface = xdg_wm_base_get_xdg_surface(instance->xdg_wm_base, window->wl_surface);
	xdg_surface_add_listener(window->xdg_surface, &xdg_surface_listener, window);
	
	window->toplevel = xdg_surface_get_toplevel(window->xdg_surface);
	xdg_toplevel_add_listener(window->toplevel, &toplevel_listener, window);
	
	wl_surface_commit(window->wl_surface);
	
	return window;
}

cwlInstance *cwlWindow_get_instance(cwlWindow *window) {
	return window->instance;
}

void cwlWindow_set_user_data(cwlWindow *window, void *data) {
	window->user_data = data;
}

void *cwlWindow_get_user_data(cwlWindow *window) {
	return window->user_data;
}

#define O(name, type) \
	void cwlWindow_##name(cwlWindow *window, type callback) { window->name = callback; }
ENUMERATE_WINDOW_CALLBACKS(O)
#undef O

void cwlWindow_destroy(cwlWindow *window) {
	if (window->instance->pointer_window == window)
		window->instance->pointer_window = NULL;
	if (window->instance->keyboard_window == window)
		window->instance->keyboard_window = NULL;
	
	xdg_toplevel_destroy(window->toplevel);
	xdg_surface_destroy(window->xdg_surface);
	wl_callback_destroy(window->frame_callback);
	wl_surface_destroy(window->wl_surface);
	free(window);
}

VkSurfaceKHR cwlWindow_create_vulkan_surface(cwlWindow *window, VkInstance vk_instance) {
	VkSurfaceKHR vk_surface;
	VkResult result = vkCreateWaylandSurfaceKHR(vk_instance, &(VkWaylandSurfaceCreateInfoKHR) {
		.sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR,
		.display = window->instance->display,
		.surface = window->wl_surface,
	}, NULL, &vk_surface);
	return result == VK_SUCCESS ? vk_surface : NULL;
}

_Bool cwlWindow_should_use_mailbox_present_mode(cwlWindow *) {
	return true;
}

void cwlWindow_begin_move(cwlWindow *window) {
	xdg_toplevel_move(window->toplevel, window->instance->seat, window->instance->event_serial);
}

void cwlWindow_set_title(cwlWindow *window, String title) {
	char c_title[title.size + 1];
	String_copy_to_c(title, c_title);
	xdg_toplevel_set_title(window->toplevel, c_title);
}

void cwlWindow_set_size(cwlWindow *, uint, uint) {}

bool cwlWindow_has_client_side_decoration(cwlWindow *) {
	return true;
}
