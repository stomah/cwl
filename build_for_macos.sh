set -e

CFLAGS="-std=c2x -c -Wall -Werror -Wextra $(pkg-config --cflags vulkan)"
clang $CFLAGS -O2 -flto -c cwl_macos.m
llvm-ar -r libcwl.a cwl_macos.o
clang $CFLAGS -c test.c
clang -framework AppKit -framework QuartzCore $(pkg-config --libs vulkan) test.o cwl_macos.o -o test
