#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <math.h>

typedef uint8_t byte;
typedef unsigned uint;
typedef cwlString String;

#define STRUCT(name) typedef struct name name; struct name
#define UNION(name) typedef union name name; union name
#define ENUM(name) typedef enum name name; enum name
#define ENUM_WITH_UNDERLYING_TYPE(name, type) enum name: type; ENUM(name): type
#define BYTE_ENUM(name) ENUM_WITH_UNDERLYING_TYPE(name, byte)

#define MIN(a, b) ({ __auto_type _a = (a); __auto_type _b = (b); _a < _b ? _a : _b; })
#define MAX(a, b) ({ __auto_type _a = (a); __auto_type _b = (b); _a > _b ? _a : _b; })

[[noreturn]] static void die(char const *format, ...) {
	va_list va_args;
	va_start(va_args);
	vfprintf(stderr, format, va_args);
	va_end(va_args);
	fputc('\n', stderr);
	abort();
}

static void *smalloc(size_t size) {
	void */*opt*/data = malloc(size);
	if (data == NULL)
		die("failed to malloc %zd bytes!", size);
	return data;
}

#define COUNTOF(arr) (sizeof(arr) / sizeof((arr)[0]))

#define eprintf(...) fprintf(stderr, __VA_ARGS__)

static inline void *memdup(void const *data, size_t size) {
	return memcpy(smalloc(size), data, size);
}

//#define STR(literal) (String) { sizeof(literal) - 1, (static char const [sizeof(literal) - 1]) {literal} }
#define STR(literal) (String) { sizeof(literal) - 1, literal }

#define String_FMT "%.*s"
#define String_PRINTF(s) (int)(s).size, (s).bytes

static inline String String_from_c(char const *s) {
	return (String) { strlen(s), s };
}

static inline bool String_eq(String a, String b) {
	return a.size == b.size && !memcmp(a.bytes, b.bytes, a.size);
}

static inline void String_copy(char *dst, String s) {
	memcpy(dst, s.bytes, s.size);
}

static inline void String_copy_to_c(String s, char *c /*[s.size + 1]*/) {
	memcpy(c, s.bytes, s.size);
	c[s.size] = 0;
}

static inline String String_clone(String s) {
	return (String) { s.size, memdup(s.bytes, s.size) };
}

// --------------------<<  bit array  >>--------------------

static inline size_t bit_array_get_word_count(size_t bit_count) {
	return (bit_count + 63) / 64;
}

static inline uint64_t *bit_array_alloc_zero(size_t bit_count) {
	size_t word_count = bit_array_get_word_count(bit_count);
	uint64_t *words = smalloc(sizeof*words * word_count);
	memset(words, 0, sizeof*words * word_count);
	return words;
}

static inline bool bit_array_get(uint64_t const *words, size_t i) {
	return words[i / 64] & 1ULL << (i % 64);
}

static inline void bit_array_set(uint64_t *words, size_t i, bool value) {
	if (value)
		words[i / 64] |= 1ULL << (i % 64);
	else
		words[i / 64] &= ~(1ULL << (i % 64));
}

// --------------------<<  CWL  >>--------------------

#define ENUMERATE_WINDOW_CALLBACKS(O) \
	O(on_close, cwlWindowCloseHandler) \
	O(on_resize, cwlWindowResizeHandler) \
	O(on_frame, cwlWindowFrameHandler) \
	O(on_pointer_move, cwlPointerMotionHandler) \
	O(on_pointer_button, cwlPointerButtonHandler) \
	O(on_pointer_axis, cwlPointerAxisHandler) \
	O(on_key, cwlKeyHandler)

static void on_close_default(cwlWindow *) {}
static void on_resize_default(cwlWindow *, uint, uint, float, cwlWindowStateFlags) {}
static void on_frame_default(cwlWindow *, uint) {}
static void on_pointer_move_default(cwlWindow *, uint, double, double) {}
static void on_pointer_button_default(cwlWindow *, uint, uint, bool) {}
static void on_pointer_axis_default(cwlWindow *, uint, double, double, cwlPointerAxisSource) {}
static void on_key_default(cwlWindow *, uint, uint, cwlKeyEventType) {}
