set -e

CFLAGS="-std=c2x -c -Wall -Werror -Wextra -D_POSIX_C_SOURCE=200809L"
clang $CFLAGS -c cwl_wayland.c xdg-shell.c
ar -r libcwl.a cwl_wayland.o xdg-shell.o
clang $CFLAGS -c test.c
clang -lwayland-client -lwayland-server -lwayland-cursor -lxkbcommon -lvulkan test.o cwl_wayland.o xdg-shell.o -o test
