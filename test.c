#include "cwl.h"

#include <stdio.h>

typedef unsigned uint;

static void on_close(cwlWindow *window) {
	cwlInstance_terminate(cwlWindow_get_instance(window));
}

static void on_resize(cwlWindow *window, uint width, uint height, float scale, cwlWindowStateFlags) {
	uint size = width == 0 ? 200 : (width + height) / 2;
	printf("resize (%u, %u), scale = %f => %u\n", width, height, scale, size);
	cwlWindow_set_size(window, size, size);
}

static void on_frame(cwlWindow *, uint time) {
	printf("%u: frame\n", time);
}

static void on_pointer_move(cwlWindow *, uint time, double x, double y) {
	printf("%u: pointer moved to (%f, %f)\n", time, x, y);
}

static void on_pointer_button(cwlWindow *, uint time, cwlPointerButton button, bool state) {
	printf("%u: pointer button %u = %d\n", time, button, state);
}

static void on_pointer_axis(
	cwlWindow *, uint time, double x, double y, cwlPointerAxisSource source)
{
	static char const *const s[] = { "unknown", "wheel", "finger", "continuous" };
	printf("%u: pointer axis (%f, %f) [%s]\n", time, x, y, s[source]);
}

static void on_key(cwlWindow *, uint time, uint key_code, cwlKeyEventType event_type) {
	static char const *const s[] = { "released", "pressed", "repeated" };
	printf("%u: key %u %s\n", time, key_code, s[event_type]);
}

int main() {
	cwlInstance *cwl = cwlInstance_create();
	
	cwlWindow *window = cwlWindow_create(cwl,
		cwlWindowFlags_RESIZABLE|cwlWindowFlags_TITLED|cwlWindowFlags_CLOSABLE, NULL);
	cwlWindow_set_title(window, cwl_STR("Window Title text"));
	
	cwlWindow_on_close(window, on_close);
	cwlWindow_on_resize(window, on_resize);
	cwlWindow_on_frame(window, on_frame);
	cwlWindow_on_pointer_move(window, on_pointer_move);
	cwlWindow_on_pointer_button(window, on_pointer_button);
	cwlWindow_on_pointer_axis(window, on_pointer_axis);
	cwlWindow_on_key(window, on_key);
	
	cwlInstance_run(cwl);
	
	cwlWindow_destroy(window);
	cwlInstance_destroy(cwl);
}
