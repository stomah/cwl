#include "cwl.h"

#include "internal_common.h"

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreVideo/CoreVideo.h>

#include <vulkan/vulkan.h>
#include <vulkan/vulkan_metal.h>

@interface Window : NSWindow<NSWindowDelegate> {
	@public cwlInstance *instance;
	@public Window */*opt*/next;
	@public Window **prev;
	
	@public CVDisplayLinkRef display_link;
	
	@public bool size_set;
	@public bool in_resize;
	@public CGSize new_size;
	
	@public void *user_data;
	
#define O(name, type) @public type name;
	ENUMERATE_WINDOW_CALLBACKS(O)
#undef O
}
@end

@interface CWLView : NSView {}
@end

STRUCT(cwlInstance) {
	Window */*opt*/first_window;
	Window */*opt*/hovered_window;
	double mouse_x, mouse_y;
};

cwlInstance *cwlInstance_create(void) {
	cwlInstance *instance = smalloc(sizeof*instance);
	*instance = (cwlInstance) {};
	[NSUserDefaults.standardUserDefaults setBool:NO forKey:@"AppleMomentumScrollSupported"];
	[NSApplication sharedApplication];
	[NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
	[NSApp activateIgnoringOtherApps:YES];
	return instance;
}

void cwlInstance_destroy(cwlInstance *instance) {
	free(instance);
}

char const *cwlInstance_get_vulkan_surface_extension(cwlInstance *) {
	return VK_EXT_METAL_SURFACE_EXTENSION_NAME;
}

void cwlInstance_run(cwlInstance *instance) {
	for (Window */*opt*/window = instance->first_window; window != NULL; window = window->next) {
		if (!window->size_set)
			window->on_resize((cwlWindow *)window, 0, 0, window.backingScaleFactor, false);
		CVDisplayLinkStart(window->display_link);
	}
	#if 0
	while (instance->run) {
		NSEvent *event = [NSApp nextEventMatchingMask:NSEventMaskAny
			untilDate:nil inMode:NSDefaultRunLoopMode dequeue:YES];
		if (event == nil) {
			uint time = get_time();
			for (Window */*opt*/window = instance->first_window; window != NULL; window = window->next)
				window->on_frame((cwlWindow *)window, time);
		} else {
			//puts("see");
			[NSApp sendEvent:event];
			//puts("see end");
		}
	}
	#endif
	[NSApp run];
}

void cwlInstance_terminate(cwlInstance *) {
	[NSApp stop:nil];
}

cwlWindow */*opt*/cwlInstance_get_hovered_window(cwlInstance *instance) {
	return (cwlWindow */*opt*/)instance->hovered_window;
}

double cwlInstance_get_pointer_x(cwlInstance *instance) {
	return instance->mouse_x;
}

double cwlInstance_get_pointer_y(cwlInstance *instance) {
	return instance->mouse_y;
}

unsigned cwlInstance_get_pressed_pointer_buttons(cwlInstance *) {
	return NSEvent.pressedMouseButtons;
}



@implementation Window

- (BOOL)windowShouldClose:(id)_sender {
	on_close((cwlWindow *)self);
	return NO;
}

/*- (void)windowDidResize:(NSNotification *)notification {
	//CGSize size = self.contentLayoutRect.size;
	//on_resize((cwlWindow *)self, size.width, size.height, self.backingScaleFactor);
	on_frame((cwlWindow *)self, get_time());
}*/

- (NSSize)windowWillResize:(NSWindow *)sender toSize:(NSSize)frame_size {
	CGSize size = [self contentRectForFrameRect:(NSRect) {{}, frame_size}].size;
	in_resize = true;
	new_size = self.contentLayoutRect.size;
	on_resize((cwlWindow *)self,
		size.width * self.backingScaleFactor, size.height * self.backingScaleFactor,
		self.backingScaleFactor,
		((self.styleMask & NSWindowStyleMaskFullScreen) ? cwlWindowStateFlags_FULLSCREEN : 0) |
		//(self.zoomed ? cwlWindowStateFlags_MAXIMIZED : 0) |
		(self.keyWindow ? cwlWindowStateFlags_ACTIVE : 0));
	in_resize = false;
	return [self frameRectForContentRect:(NSRect) {{}, new_size}].size;
}

- (void)windowDidChangeScreen:(NSNotification *)notification {
	NSNumber *display_id_number = [self.screen.deviceDescription objectForKey:@"NSScreenNumber"];
	CGDirectDisplayID display_id = display_id_number.unsignedIntValue;
	if (CVDisplayLinkSetCurrentCGDisplay(display_link, display_id) != kCVReturnSuccess)
		die("failed to set CGDisplayLink current display");
}

- (void)windowDidChangeBackingProperties:(NSNotification *)notification {
	NSNumber *old_scale = [notification.userInfo objectForKey:NSBackingPropertyOldScaleFactorKey];
	if (self.backingScaleFactor != old_scale.doubleValue) {
		CGSize size = self.contentLayoutRect.size;
		on_resize((cwlWindow *)self,
			size.width * self.backingScaleFactor, size.height * self.backingScaleFactor,
			self.backingScaleFactor,
			((self.styleMask & NSWindowStyleMaskFullScreen) ? cwlWindowStateFlags_FULLSCREEN : 0) |
			(self.keyWindow ? cwlWindowStateFlags_ACTIVE : 0));
	}
}

- (void)mouseEntered:(NSEvent *)event {
	puts("enter");
}

- (void)mouseMoved:(NSEvent *)event {
	NSPoint pos_in_window = event.locationInWindow;
	NSRect content_rect = self.contentLayoutRect;
	on_pointer_move((cwlWindow *)self, event.timestamp * 1000,
		pos_in_window.x * self.backingScaleFactor,
		(content_rect.size.height - pos_in_window.y) * self.backingScaleFactor);
}

- (void)mouseDragged:(NSEvent *)event { [self mouseMoved:event]; }
- (void)rightMouseDragged:(NSEvent *)event { [self mouseMoved:event]; }
- (void)otherMouseDragged:(NSEvent *)event { [self mouseMoved:event]; }

- (void)mouseButton:(NSEvent *)event state:(bool)state {
	// event.buttonNumber happens to correspond to cwlPointerButton
	on_pointer_button((cwlWindow *)self, event.timestamp * 1000, event.buttonNumber, state);
}

- (void)mouseDown:(NSEvent *)event { [self mouseButton:event state:true]; }
- (void)mouseUp:(NSEvent *)event { [self mouseButton:event state:false]; }
- (void)rightMouseDown:(NSEvent *)event { [self mouseButton:event state:true]; }
- (void)rightMouseUp:(NSEvent *)event { [self mouseButton:event state:false]; }
- (void)otherMouseDown:(NSEvent *)event { [self mouseButton:event state:true]; }
- (void)otherMouseUp:(NSEvent *)event { [self mouseButton:event state:false]; }

- (void)scrollWheel:(NSEvent *)event {
	on_pointer_axis((cwlWindow *)self, event.timestamp * 1000,
		-event.scrollingDeltaX, -event.scrollingDeltaY,
		event.hasPreciseScrollingDeltas ?
			cwlPointerAxisSource_FINGER : cwlPointerAxisSource_CONTINUOUS);
}

- (void)keyDown:(NSEvent *)event {
	on_key((cwlWindow *)self, event.timestamp * 1000, event.keyCode,
		event.ARepeat ? cwlKeyEventType_REPEAT : cwlKeyEventType_PRESS);
}

- (void)keyUp:(NSEvent *)event {
	on_key((cwlWindow *)self, event.timestamp * 1000, event.keyCode, cwlKeyEventType_RELEASE);
}

@end

@implementation CWLView

- (BOOL)acceptsFirstMouse:(NSEvent *)event {
	return YES;
}

@end

/*static void print_time(CVTimeStamp const *time, char const *label) {
	printf("%s, flags=%#llx host=%llu rate_sc=%f video=%llu scale=%d t=%u\n",
		label,
		time->flags,
		time->hostTime / (time->videoTimeScale / 1000),
		time->rateScalar,
		time->videoTime / (time->videoTimeScale / 1000),
		time->videoTimeScale,
		get_time());
}*/

static CVReturn on_frame(
	CVDisplayLinkRef, CVTimeStamp const *now, CVTimeStamp const */*output_time*/,
	CVOptionFlags, CVOptionFlags *, void *data)
{
	Window *window = data;
	//printf("frame:\n");
	//print_time(now, "now");
	//print_time(output_time, "out");
	//window.contentView.needsDisplay = YES;
	uint timestamp = now->hostTime * 1000 / now->videoTimeScale;
	dispatch_async(dispatch_get_main_queue(), ^{
		if (window.isVisible)
			window->on_frame((cwlWindow *)window, timestamp);
	});
	return kCVReturnSuccess;
}

cwlWindow *cwlWindow_create(cwlInstance *instance, cwlWindowFlags flags, void *user_data) {
	NSWindowStyleMask ns_style = NSWindowStyleMaskMiniaturizable;
	if (flags & cwlWindowFlags_RESIZABLE)
		ns_style |= NSWindowStyleMaskResizable;
	if (flags & cwlWindowFlags_TITLED)
		ns_style |= NSWindowStyleMaskTitled;
	else
		ns_style |= NSWindowStyleMaskBorderless;
	if (flags & cwlWindowFlags_CLOSABLE)
		ns_style |= NSWindowStyleMaskClosable;
	
	Window *window = [[Window alloc]
		initWithContentRect:NSMakeRect(0, 0, 0, 0)
		styleMask:ns_style
		backing:NSBackingStoreBuffered
		defer:NO];
	window->instance = instance;
	
	window->next = instance->first_window;
	if (window->next != NULL)
		window->next->prev = &window->next;
	window->prev = &instance->first_window;
	instance->first_window = window;
	
	NSNumber *display_id_number = [window.screen.deviceDescription objectForKey:@"NSScreenNumber"];
	CGDirectDisplayID display_id = display_id_number.unsignedIntValue;
	if (CVDisplayLinkCreateWithCGDisplay(display_id, &window->display_link) != kCVReturnSuccess)
		die("failed to create display link");
	CVDisplayLinkSetOutputCallback(window->display_link, on_frame, window);
	
	window->user_data = user_data;
	window->size_set = false;
	window->in_resize = false;
	
#define O(name, type) window->name = name##_default;
	ENUMERATE_WINDOW_CALLBACKS(O)
#undef O
	
	NSView *view = [CWLView new];
	CAMetalLayer *layer = [CAMetalLayer layer];
	view.layer = layer;
	view.wantsLayer = YES;
	window.contentView = view;
	
	[window setDelegate:window];
	window.acceptsMouseMovedEvents = YES;
	[window cascadeTopLeftFromPoint:NSMakePoint(20,20)];
	[window makeKeyAndOrderFront:nil];
	return (cwlWindow *)window;
}

cwlInstance *cwlWindow_get_instance(cwlWindow *window_) {
	Window *window = (Window *)window_;
	return window->instance;
}

void cwlWindow_set_user_data(cwlWindow *window_, void *data) {
	Window *window = (Window *)window_;
	window->user_data = data;
}

void *cwlWindow_get_user_data(cwlWindow *window_) {
	Window *window = (Window *)window_;
	return window->user_data;
}

VkSurfaceKHR cwlWindow_create_vulkan_surface(cwlWindow *window_, VkInstance vk_instance) {
	Window *window = (Window *)window_;
	VkSurfaceKHR vk_surface;
	VkResult result = vkCreateMetalSurfaceEXT(vk_instance, &(VkMetalSurfaceCreateInfoEXT) {
		.sType = VK_STRUCTURE_TYPE_METAL_SURFACE_CREATE_INFO_EXT,
		.pLayer = (CAMetalLayer *)window.contentView.layer,
	}, NULL, &vk_surface);
	return result == VK_SUCCESS ? vk_surface : NULL;
}

_Bool cwlWindow_should_use_mailbox_present_mode(cwlWindow *) {
	return false;
}

void cwlWindow_set_title(cwlWindow *window, String title) {
	@autoreleasepool {
		[(Window *)window setTitle:[[NSString alloc]
			initWithBytes:title.bytes length:title.size encoding:NSUTF8StringEncoding]];
	}
}

void cwlWindow_set_size(cwlWindow *window_, uint width, uint height) {
	Window *window = (Window *)window_;
	// FIXME: rounding
	width /= window.backingScaleFactor;
	height /= window.backingScaleFactor;
	if (window->in_resize)
		window->new_size = (CGSize) {width, height};
	else
		[window setContentSize:NSMakeSize(width, height)];
	window->size_set = true;
}

_Bool cwlWindow_has_client_side_decoration(cwlWindow *) {
	return false;
}

void cwlWindow_begin_move(cwlWindow *) {}

#define O(name, type) \
	void cwlWindow_##name(cwlWindow *window, type callback) { ((Window *)window)->name = callback; }
ENUMERATE_WINDOW_CALLBACKS(O)
#undef O

void cwlWindow_destroy(cwlWindow *window_) {
	Window *window = (Window *)window_;
	*window->prev = window->next;
	if (window->next != NULL)
		window->next->prev = window->prev;
	CVDisplayLinkRelease(window->display_link);
	[window close];
}
